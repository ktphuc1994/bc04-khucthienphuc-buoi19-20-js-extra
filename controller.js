let isPrimeNum = (num) => {
  if (num <= 1) {
    return false;
  } else {
    // for (var count = 2; num % count != 0; count++);
    // return count == num;
    for (let count = 2; count <= Math.sqrt(num); count++) {
      if (num % count == 0) {
        return false;
      }
    }
  }
  return true;
};
