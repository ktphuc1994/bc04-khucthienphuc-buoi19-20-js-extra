let checkNumberStr = (arr, whereToShow, message) => {
  const numberArrFormat = /^(\s*-?\s*[0-9]+\s*)(,\s*-?\s*[0-9]+\s*)*$/;
  if (arr.match(numberArrFormat)) {
    document.getElementById(whereToShow).innerText = "";
    return true;
  } else {
    document.getElementById(whereToShow).innerText = message;
    return false;
  }
};

let checkPositiveInteger = (value, errIdAddress, message) => {
  if (value.match(/^\d+$/)) {
    document.getElementById(errIdAddress).innerText = "";
    return true;
  } else {
    document.getElementById(errIdAddress).innerText = message;
    return false;
  }
};

let checkHours = (value, errIdAddress, message) => {
  if (value.match(/^\d+$/) && value * 1 >= 1 && value * 1 <= 12) {
    document.getElementById(errIdAddress).innerText = "";
    return true;
  } else {
    document.getElementById(errIdAddress).innerText = message;
    return false;
  }
};

let checkMinutes = (value, errIdAddress, message) => {
  if (value.match(/^\d+$/) && value * 1 >= 0 && value * 1 <= 59) {
    document.getElementById(errIdAddress).innerText = "";
    return true;
  } else {
    document.getElementById(errIdAddress).innerText = message;
    return false;
  }
};
