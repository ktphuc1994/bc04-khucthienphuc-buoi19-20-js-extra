// START EX01
document.getElementById("ex01-inBang").onclick = () => {
  //   console.log("yes");
  document.getElementById("ex01-result").innerHTML = "";
  let count = 1;
  for (let i = 0; i < 10; i++) {
    let newTR = document.createElement("tr");
    let innerTR = "";
    let upperIndexValue = count + 10;
    for (count; count < upperIndexValue; count++) {
      innerTR += `<td>${count}</td>`;
    }
    newTR.innerHTML = innerTR;
    document.getElementById("ex01-result").append(newTR);
  }
};
// END EX01

// START EX02
document.getElementById("ex02-inSoNguyenTo").onclick = () => {
  // console.log("yes");
  let soNguyenStr = document.getElementById("txt-ex02-mangSoNguyen").value;
  isValid = checkNumberStr(
    soNguyenStr,
    "ex02-errNote",
    "Mảng phải là số nguyên, Cách nhau bởi dấu ','"
  );
  if (isValid) {
    soNguyenStr = soNguyenStr.replace(/\s/g, "");
    let soNguyenArr = soNguyenStr.split(",");
    soNguyenArr = soNguyenArr.map((soNguyen) => (soNguyen *= 1));
    let soNguyenToArr = soNguyenArr.filter((num) => isPrimeNum(num) == true);
    if (soNguyenToArr.length == 0) {
      document.getElementById(
        "ex02-result"
      ).innerText = `Mảng không có số Nguyên tố`;
    } else {
      document.getElementById(
        "ex02-result"
      ).innerHTML = `<p>Các số Nguyên Tố có trong mảng:</p>
    <p class="fs-4 fw-semibold">[${soNguyenToArr.join(", ")}]</p>`;
    }
  }
};
// END EX02

// START EX03
document.getElementById("ex03-tinhTongS").onclick = () => {
  let thamSoN = document.getElementById("txt-ex03-n").value;
  if (thamSoN.match(/^\d+$/) && thamSoN * 1 > 1) {
    document.getElementById("ex03-errNote").innerText = "";
    thamSoN *= 1;
    let totalS = 0;
    for (let i = 2; i <= thamSoN; i++) {
      totalS += i;
    }
    totalS += 2 * thamSoN;
    document.getElementById("ex03-result").innerText = `Tổng S = ${totalS}`;
  } else {
    document.getElementById(
      "ex03-errNote"
    ).innerText = `n phải là số Nguyên DƯƠNG, và lớn hơn 1`;
    document.getElementById("ex03-result").innerText = "";
  }
};
// END EX03

// START EX04
document.getElementById("ex04-tinhTongUocSo").onclick = () => {
  let thamSoN = document.getElementById("txt-ex04-n").value;
  if (thamSoN.match(/^\d+$/) && thamSoN * 1 > 0) {
    document.getElementById("ex04-errNote").innerText = "";
    thamSoN *= 1;
    let uocNArr = [];
    for (let i = 1; i <= thamSoN; i++) {
      thamSoN % i == 0 && uocNArr.push(i);
    }
    document.getElementById(
      "ex04-result"
    ).innerHTML = `<p>Tổng số lượng ước của ${thamSoN} là ${uocNArr.length}.</p>
    <p>Các ước đó là: ${uocNArr.join(", ")}</p>`;
  } else {
    document.getElementById(
      "ex04-errNote"
    ).innerText = `n phải là số Nguyên DƯƠNG`;
    document.getElementById("ex04-result").innerText = "";
  }
};
// END EX04

// START EX05
document.getElementById("ex05-daoNguoc").onclick = () => {
  let thamSoN = document.getElementById("txt-ex05-n").value;
  if (thamSoN.match(/^\d+$/) && thamSoN * 1 > 0) {
    document.getElementById("ex05-errNote").innerText = "";
    let arrFromN = thamSoN.split("");
    arrFromN.reverse();
    let reverseN = "";
    arrFromN.forEach((digit) => (reverseN += digit));
    document.getElementById(
      "ex05-result"
    ).innerText = `Số đảo ngược của ${thamSoN} là: ${reverseN}`;
  } else {
    document.getElementById(
      "ex05-errNote"
    ).innerText = `n phải là số Nguyên DƯƠNG`;
    document.getElementById("ex05-result").innerText = "";
  }
};
// END EX05

// START EX06
document.getElementById("ex06-timX").onclick = () => {
  let thamSoN = document.getElementById("txt-ex06-n").value;
  if (thamSoN.match(/^\d+$/) && thamSoN * 1 > 0) {
    document.getElementById("ex06-errNote").innerText = "";
    thamSoN *= 1;
    let totalOfX = 0;
    let trueX = null;
    for (let i = 1; totalOfX <= thamSoN; i++) {
      totalOfX += i;
      trueX = i;
    }
    document.getElementById(
      "ex06-result"
    ).innerText = `X lớn nhất thỏa mãn điều kiện bài toán là: x = ${trueX - 1}`;
  } else {
    document.getElementById(
      "ex06-errNote"
    ).innerText = `n phải là số Nguyên DƯƠNG`;
    document.getElementById("ex06-result").innerText = "";
  }
};
// END EX06

// START EX07
document.getElementById("ex07-inCuuChuong").onclick = () => {
  let thamSoN = document.getElementById("txt-ex07-n").value;
  if (thamSoN.match(/^\d+$/) && thamSoN * 1 > 0) {
    document.getElementById("ex07-errNote").innerText = "";
    thamSoN *= 1;
    let bangCuuChuong = "";
    for (let i = 0; i < 11; i++) {
      bangCuuChuong += `<p class="mb-2">${thamSoN} x ${i} = ${thamSoN * i}</p>`;
    }
    document.getElementById("ex07-result").innerHTML = bangCuuChuong;
  } else {
    document.getElementById(
      "ex07-errNote"
    ).innerText = `n phải là số Nguyên DƯƠNG`;
    document.getElementById("ex07-result").innerText = "";
  }
};
// END EX07

// START EX08
let cards = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];
let players = Array.from(Array(4), () => []);
let dealtCards = 0;
document.getElementById("ex08-chiaBai").onclick = () => {
  if (dealtCards < 12) {
    for (let i = 0; i < 4; i++) {
      players[i].push(cards[0]);
      dealtCards++;
      cards.splice(0, 1);
    }
    document.getElementById("ex08-cards").innerText = cards.join(", ");
    document.getElementById("ex08-player1").innerText = players[0].join(", ");
    document.getElementById("ex08-player2").innerText = players[1].join(", ");
    document.getElementById("ex08-player3").innerText = players[2].join(", ");
    document.getElementById("ex08-player4").innerText = players[3].join(", ");
  } else {
    document.getElementById("ex08-errNote").innerText = `Đã hết bài`;
  }
};
document.getElementById("ex08-resetCards").onclick = () => {
  cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  players = Array.from(Array(4), () => []);
  dealtCards = 0;
  document.getElementById("ex08-cards").innerText =
    "4K, KH, 5C, KA, QH, KD, 2H, 10S, AS, 7H, 9K, 10D";
  document.getElementById("ex08-player1").innerText = "";
  document.getElementById("ex08-player2").innerText = "";
  document.getElementById("ex08-player3").innerText = "";
  document.getElementById("ex08-player4").innerText = "";
  document.getElementById("ex08-errNote").innerText = "";
};

// END EX08

// START EX09
document.getElementById("ex09-timChoGa").onclick = () => {
  let totalDogChick = document.getElementById("txt-ex09-soCon").value;
  let totalLeg = document.getElementById("txt-ex09-soChan").value;
  let isValid =
    checkPositiveInteger(
      totalDogChick,
      "ex09-errNote-soCon",
      "Số con phải là số Nguyên Dương"
    ) &
    checkPositiveInteger(
      totalLeg,
      "ex09-errNote-soChan",
      "Số chân phải là số Nguyên Dương"
    );
  if (isValid) {
    totalDogChick *= 1;
    totalLeg *= 1;
    let chickenArr = [];
    let dogArr = [];
    let totalDog = null;
    let totalChicken = null;
    for (let i = 0; i <= totalDogChick; i++) {
      chickenArr.push(i);
      dogArr.push(i);
    }
    for (let i = 0; i < chickenArr.length; i++) {
      let dogIndex = dogArr.findIndex(
        (dog) => dog * 4 + chickenArr[i] * 2 == totalLeg
      );
      if (dogIndex != -1 && dogArr[dogIndex] + chickenArr[i] == totalDogChick) {
        totalDog = dogArr[dogIndex];
        totalChicken = chickenArr[i];
        break;
      }
    }
    if (totalDog == null) {
      document.getElementById(
        "ex09-result-soCho"
      ).innerText = `Không có lượng Chó, Gà nào thỏa điều kiện số Chân & số Con`;
      document.getElementById("ex09-result-soGa").innerText = "";
    } else {
      document.getElementById(
        "ex09-result-soCho"
      ).innerText = `Số chó: ${totalDog}`;
      document.getElementById(
        "ex09-result-soGa"
      ).innerText = `Số gà: ${totalChicken}`;
    }
  }
};
// END EX09

// START EX10
document.getElementById("ex10-tinhGoc").onclick = () => {
  let hours = document.getElementById("txt-ex10-hours").value;
  let minutes = document.getElementById("txt-ex10-minutes").value;
  let isValid =
    checkHours(hours, "ex10-errNote-hours", "Giờ là số nguyên, từ 1 đến 12") &
    checkMinutes(
      minutes,
      "ex10-errNote-minutes",
      "Phút là số nguyên, từ 0 đến 59"
    );
  if (isValid) {
    /**
     * Góc kim giờ so với 12h: gg
     * gg = hours/12 * 360
     *
     * Góc kim phút so với 00p: gp
     * gp = minutes/60 * 360.
     *
     * Góc kim giờ di chuyển thêm theo phút: gtp
     * gtp = minutes*(1/12*360)/60
     *
     * Góc kim giờ thật sự so với 12h: ggreal
     * ggreal = gg+gtp;
     *
     * Góc giữ kim giờ & kim phút: Trị tuyệt đối (gp-ggreal)
     */
    hours *= 1;
    minutes *= 1;
    let hoursAngle = (hours / 12) * 360 + (minutes * ((1 / 12) * 360)) / 60;
    let minutesAngle = (minutes / 60) * 360;
    let hoursMinutesAngle = Math.abs(hoursAngle - minutesAngle);
    document.getElementById(
      "ex10-result"
    ).innerHTML = `Góc giữa kim giờ và kim phút là: ${hoursMinutesAngle}<sup>o</sup> hoặc ${
      360 - hoursMinutesAngle
    }<sup>o</sup>`;
  }
};
// END EX10
